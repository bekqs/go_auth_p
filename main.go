package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/appengine"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var err error
var state string
var store = sessions.NewCookieStore([]byte("SF2aGlBoxrDi4ovP35cEscVWg2t2IW6i"))

// User details for registration
type User struct {
	ID       int
	Email    string
	Password string
}

// ProfileData - User profile details
type ProfileData struct {
	Email        string
	FullName     string
	ContactEmail string
	Address      string
	Phone        string
}

// Connect to Database
func connectDB() {
	db, err = sql.Open("mysql", "root:rootroot@/go_db")

	if err != nil {
		log.Fatalln(err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatalln(err)
	}
}

func routes() {
	r := mux.NewRouter().StrictSlash(true)

	r.HandleFunc("/", index)
	r.HandleFunc("/register", register)
	r.HandleFunc("/profile", profile)
	r.HandleFunc("/login", login)
	r.HandleFunc("/logout", logout)
	http.Handle("/", r)
	r.PathPrefix("/css/").Handler(http.StripPrefix("/css/", http.FileServer(http.Dir("dist/css/"))))
	r.PathPrefix("/images/").Handler(http.StripPrefix("/images/", http.FileServer(http.Dir("dist/images/"))))
	r.PathPrefix("/dist/").Handler(http.StripPrefix("/dist/", http.FileServer(http.Dir("dist/"))))

	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:8080",
	}

	fmt.Println("Server running on port :8080")
	log.Fatal(srv.ListenAndServe())
}

func outputHTML(w http.ResponseWriter, filename string, data interface{}) {
	t, err := template.ParseFiles(filename)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if err := t.Execute(w, data); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}

func checkErr(w http.ResponseWriter, r *http.Request, err error) bool {
	if err != nil {
		fmt.Println(r.Host + r.URL.Path)
		http.Redirect(w, r, r.Host+r.URL.Path, 301)
		return false
	}
	return true
}

// User in database
func queryUser(email string) User {
	var users = User{}
	err = db.QueryRow(`
		SELECT id, 
		email, 
		password 
		FROM users WHERE email=?
		`, email).
		Scan(
			&users.ID,
			&users.Email,
			&users.Password,
		)
	return users
}

var data map[string]string

func index(w http.ResponseWriter, r *http.Request) {
	// Start session if user is logged in
	session, _ := store.Get(r, "current-session")
	sessionEmail := session.Values["email"]

	// If user is not authenticated redirect to login page
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Redirect(w, r, "/login", 302)
		return
	}

	var p ProfileData

	// Get user profile details from database
	err = db.QueryRow("SELECT email, full_name, contact_email, address, phone FROM users where email = ?", sessionEmail).Scan(&p.Email, &p.FullName, &p.ContactEmail, &p.Address, &p.Phone)
	if err != nil {
		fmt.Println(err.Error())
	}

	// Data for HTML
	data = map[string]string{
		"email":        session.Values["email"].(string),
		"fullName":     p.FullName,
		"contactEmail": p.ContactEmail,
		"address":      p.Address,
		"phone":        p.Phone,
	}

	// Execute HTML template with data
	outputHTML(w, "templates/index.html", data)
}

func register(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/register.html")
		return
	}

	// Get values from the form
	email := r.FormValue("email")
	password := r.FormValue("password")
	passwordConfirm := r.FormValue("password_confirm")
	users := queryUser(email)

	if (User{}) == users {
		if len(password) != 0 && password == passwordConfirm {
			// Hash password
			hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

			// If there is no error save user details to database
			if len(hashedPassword) != 0 && checkErr(w, r, err) {
				stmt, err := db.Prepare("INSERT INTO users SET email=?, password=?")
				if err == nil {
					session, _ := store.Get(r, "current-session")

					if err != nil {
						http.Error(w, err.Error(), http.StatusInternalServerError)
						return
					}

					_, err := stmt.Exec(&email, &hashedPassword)

					// Start session after sign up
					session.Values["email"] = &email
					session.Values["authenticated"] = true
					session.Save(r, w)

					// Redirect to Edit profile page
					http.Redirect(w, r, "/profile", 302)

					if err != nil {
						http.Error(w, err.Error(), http.StatusInternalServerError)
						return
					}
				}
			}
		} else {
			http.Redirect(w, r, "/register", 302)
		}
	} else {
		http.Redirect(w, r, "/register", 302)
	}
}

func profile(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		// Execute HTML template with data
		outputHTML(w, "templates/profile.html", data)
		return
	}
	// Get session value
	session, _ := store.Get(r, "current-session")
	sessionEmail := session.Values["email"]

	// Get values from the form
	fullName := r.FormValue("full_name")
	contactEmail := r.FormValue("contact_email")
	address := r.FormValue("address")
	phone := r.FormValue("phone")

	stmt, err := db.Prepare("UPDATE users SET full_name=?, contact_email=?, address=?, phone=? WHERE email=?")
	if err == nil {
		_, err := stmt.Exec(&fullName, &contactEmail, &address, &phone, &sessionEmail)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Redirect to index page
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/login.html")
		return
	}

	// Generate random token for session state
	session, _ := store.Get(r, "current-session")

	// Form values
	email := r.FormValue("email")
	password := r.FormValue("password")
	users := queryUser(email)

	// Check if hashed password matches to its plaintext equivalent
	var hashedPassword = bcrypt.CompareHashAndPassword([]byte(users.Password), []byte(password))

	if hashedPassword == nil {
		// Login success
		session.Values["email"] = users.Email
		session.Values["authenticated"] = true
		session.Save(r, w)
		http.Redirect(w, r, "/", 302)
	} else {
		http.Redirect(w, r, "/register", 301)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	// Revoke users authentication
	session, _ := store.Get(r, "current-session")
	session.Values["email"] = ""
	session.Values["authenticated"] = false
	session.Save(r, w)
	http.Redirect(w, r, "/", 302)
}

func main() {
	connectDB()
	routes()

	appengine.Main()

	defer db.Close()
}
